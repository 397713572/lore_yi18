 Ext.define('LoreApp.model.LoreModel', {
     extend: 'Ext.data.Model',
     config: {
     	
     	

    	 fields: [ 
    	 	{name: 'id', type: 'auto'},
    	    {name: 'title', type: 'string'},
    	    {name: 'loreclass ', type: 'string'},
    	    {name: 'className', type: 'string'},
    	   {name: 'count', type: 'int'},
    	   {name:'message',type:'string'},
    	  {name:'img',type:'string'},
    	  {name:'time',type:'date'},
    	  {name:'author',type:'string'}
    	 ]
     }
 });