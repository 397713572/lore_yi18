 Ext.define('LoreApp.model.ClassModel', {
     extend: 'Ext.data.Model',
     config: {
     	
     	

    	 fields: [ 
    	 	{name: 'id', type: 'auto'},
    	    {name: 'name', type: 'string'}

    	 ]
     }
 });