
Ext.define('LoreApp.store.LoreInfoStore',{
	extend:'Ext.data.Store',
	config: {
		 model: 'LoreApp.model.LoreModel',
		
	
        //filter the data using the firstName field
//        sorters : [{
//						property : 'id',
//						direction : 'desc'
//					}],

            //autoload the data from the server
			
       		//autoLoad: false,
        	 proxy: {
               		 type: 'jsonp',
              	 	 url: Global.Website+'lore/show',
               		 reader: {
				            type: "json",
				            rootProperty: "yi18"
				        }
           	 },
					
			
	    listeners : {
	    	
	    	beforeload : {
					fn : function(store, options) {
							var id = store.getStoreId()  ;
							
							store.getProxy().setExtraParam('id', id);
					}
	    	}
		}
	}
});