
Ext.define('LoreApp.store.SearchListStore',{
	extend:'Ext.data.Store',
	config: {
		 model: 'LoreApp.model.SearchModel',
	
        //filter the data using the firstName field
       
            //autoload the data from the server
			
       		autoLoad: false,
        	 proxy: {
               		 type: 'jsonp',
              	 	 url: Global.Website+'lore/search',
               		 reader: {
				            type: "json",
				            rootProperty: "yi18"
				        }
           	 }
					

	}
});